package com.amir.mvpdesignpattern.Home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.amir.mvpdesignpattern.Detail.DetailActivity;
import com.amir.mvpdesignpattern.Model.Retrofit.ApiClient;
import com.amir.mvpdesignpattern.Model.Retrofit.ApiService;
import com.amir.mvpdesignpattern.Model.Retrofit.Product;
import com.amir.mvpdesignpattern.R;

import java.util.List;

public class MainActivity extends AppCompatActivity implements HomeContract.View {

    private HomeContract.Presenter homePresenter;
    RecyclerView rvMain;
    HomeRvAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        homePresenter = new HomePresenter(ApiClient.getClient().create(ApiService.class));
        homePresenter.onAttach(MainActivity.this);

        rvMain = findViewById(R.id.rvMain);
        rvMain.setLayoutManager(new LinearLayoutManager(MainActivity.this));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        homePresenter.onDetach();
    }


    @Override
    public void showProductList(List<Product> products) {
        adapter = new HomeRvAdapter(products, new HomeRvAdapter.OnProductItemClick() {
            @Override
            public void onItemCLick(String id) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
        rvMain.setAdapter(adapter);
    }
}