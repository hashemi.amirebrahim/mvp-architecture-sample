package com.amir.mvpdesignpattern.Home;


import android.util.Log;
import android.widget.Toast;

import com.amir.mvpdesignpattern.BaseView;
import com.amir.mvpdesignpattern.Model.Retrofit.ApiService;
import com.amir.mvpdesignpattern.Model.Retrofit.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//این اون کلاسیه که قراره به ویو ما بگه فلان کارو بکن فلان کارو نکن
public class HomePresenter implements HomeContract.Presenter {

    private static final String TAG = "HomePresenter";
    private ApiService apiService;
    private HomeContract.View view;

    public HomePresenter(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public void onAttach(HomeContract.View view) {
        this.view = view;
        getProductList();
    }

    @Override
    public void onDetach() {
        view = null;
    }

    @Override
    public void getProductList() {
        apiService.getProductList().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                view.showProductList(response.body());
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.i(TAG, "onFailure: " + t.toString());
            }
        });
    }
}
