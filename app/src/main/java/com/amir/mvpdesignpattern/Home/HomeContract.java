package com.amir.mvpdesignpattern.Home;

import com.amir.mvpdesignpattern.BasePresenter;
import com.amir.mvpdesignpattern.BaseView;
import com.amir.mvpdesignpattern.Model.Retrofit.Product;

import java.util.List;

public interface HomeContract {
    interface View extends BaseView {
        void showProductList(List<Product> products);
    }

    interface Presenter extends BasePresenter<View> {

        void getProductList();

    }
}
