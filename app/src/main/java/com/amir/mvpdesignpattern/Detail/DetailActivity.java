package com.amir.mvpdesignpattern.Detail;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.amir.mvpdesignpattern.Model.Retrofit.ApiClient;
import com.amir.mvpdesignpattern.Model.Retrofit.ApiService;
import com.amir.mvpdesignpattern.Model.Retrofit.DetailProduct;
import com.amir.mvpdesignpattern.R;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity implements DetailContract.View {

    DetailContract.Presenter presenter;
    private String id;
    ImageView imgDetailProduct;
    TextView tvTitle, tvPrice, tvIntroduction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        presenter = new DetailPresenter(ApiClient.getClient().create(ApiService.class));
        presenter.onAttach(DetailActivity.this);
        getProductId();
        setUpViews();
    }

    private void setUpViews() {
        imgDetailProduct = findViewById(R.id.imgDetailProduct);
        tvTitle = findViewById(R.id.tvDetailTitle);
        tvPrice = findViewById(R.id.tvDetailPrice);
        tvIntroduction = findViewById(R.id.tvDetailIntroduction);
    }

    @Override
    public void showProductDetail(DetailProduct detailProduct) {
        Picasso.get().load(detailProduct.getImage()).into(imgDetailProduct);
        tvTitle.setText(detailProduct.getTitle());
        tvPrice.setText(detailProduct.getPrice());
        tvIntroduction.setText(Html.fromHtml(detailProduct.getIntroduction()));
    }

    @Override
    public void getProductId() {
        id = getIntent().getStringExtra("id");
        presenter.getDetailProduct(id);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }
}