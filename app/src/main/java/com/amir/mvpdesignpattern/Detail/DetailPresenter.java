package com.amir.mvpdesignpattern.Detail;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.amir.mvpdesignpattern.BaseView;
import com.amir.mvpdesignpattern.Model.Retrofit.ApiService;
import com.amir.mvpdesignpattern.Model.Retrofit.DetailProduct;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPresenter implements DetailContract.Presenter {

    private static final String TAG = "DetailPresenter";
    private DetailContract.View view;
    private ApiService apiService;
    public DetailPresenter(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public void onAttach(DetailContract.View view) {
        this.view = view;
    }

    @Override
    public void onDetach() {

    }


    @Override
    public void getDetailProduct(String id) {
        apiService.getDetailProduct(id, "1").enqueue(new Callback<List<DetailProduct>>() {
            @Override
            public void onResponse(Call<List<DetailProduct>> call, Response<List<DetailProduct>> response) {
                assert response.body() != null;
                view.showProductDetail(response.body().get(0));
            }

            @Override
            public void onFailure(Call<List<DetailProduct>> call, Throwable t) {
                Log.i(TAG, "onFailure: " + t.toString());
                //Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
