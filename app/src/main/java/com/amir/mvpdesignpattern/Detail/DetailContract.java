package com.amir.mvpdesignpattern.Detail;

import com.amir.mvpdesignpattern.BasePresenter;
import com.amir.mvpdesignpattern.BaseView;
import com.amir.mvpdesignpattern.Model.Retrofit.DetailProduct;

public interface DetailContract {

    interface View extends BaseView {
        void showProductDetail(DetailProduct detailProduct);
        void getProductId();
    }

    interface Presenter extends BasePresenter<View> {
        void getDetailProduct(String id);
    }

}
