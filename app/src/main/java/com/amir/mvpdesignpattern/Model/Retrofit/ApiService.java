package com.amir.mvpdesignpattern.Model.Retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("readamazing.php")
    Call<List<Product>> getProductList();

    @GET("getdetail.php")
    Call<List<DetailProduct>> getDetailProduct(@Query("id") String id, @Query("user") String user);
    //Call<List<DetailProduct>> getDetailProduct(@Path("id") String id, @Path("user") String user);
}
